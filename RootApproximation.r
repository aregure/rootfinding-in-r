# R script for root approximation with either bisection,
#   Newton's, or secant method.
# This script requires the numDeriv package installed

# Represent Euler's constant (e = 2.718281828) with R's exp() function
e <- exp(1)

# Function for bisection method of root approximation
bisection <- function(f, a, b, tol, n = 10000) {
  # Boolean value to flag if approx. root is found
  foundRoot <- FALSE
  
  # If a is greater than b, it's an invalid interval.
  #   Stop executing and return message.
  if (a >= b) {
    cat("a < b is not satisfied\n")
    return()
  }

  # If the signs of f(a) and f(b) in the evaluated interval [a, b] are same,
  #     stop the function and return message.
  if (sign(f(a)) * sign(f(b)) > 0) {
    cat("f(a) and f(b) are of the same sign\n")
    cat("Either there is no root in interval, or there are multiple roots.\n")
    cat("Please try again with a smaller interval\n")
    return()
  }
 
  # Begin calculating and displaying table
  cat("n\ta\t\tb\t\tc\t\tb - c\t\tf(c)\n")

  for (i in 1:n) {
    # Calculate the midpoint
    c <- (a + b) / 2
    cat(paste(i, "\t",
              format(a, nsmall = 9), "\t",
              format(b, nsmall = 9), "\t",
              format(c, nsmall = 9), "\t",
              format(b - c, nsmall = 9), "\t",
              format(f(c), nsmall = 9), "\n"))

    # If function is 0 at midpoint or if midpoint is lesser than tolerance,
    #     we have found an approx. root.
    # Stop the function and display the root.
    if ((f(c) == 0) || ((b - a) / 2) < tol) {
      foundRoot <- TRUE
      cat("\napprox. root = ", format(c, nsmall = 9), "\n")
      cat("Accept c = ", format(c, nsmall = 9), " as root of f(x)\n")
      return()
    }
    
    # Otherwise, another iteration might be required. 
    # Determine function signs at points c and a and assign either a or b
    #   as the midpoint for the next iteration.
    ifelse (sign(f(c)) == sign(f(a)), 
           a <- c,
           b <- c)
  }
  # If the max number of iterations is reached and no approx. root has, 
  #   been found, return message and end function.
  if (foundRoot == FALSE) {
    stop("Too many iterations reached without finding approx. root\n")
  }
}

# Function for Newton's method of root approximation
newton <- function(f, a, tol, n = 10000) {
  # We'll need the numDeriv package to get the derivative of f(x)
  require(numDeriv)
  
  # Initialize variables
  foundRoot <- FALSE	# bool variable to flag if root is found
  x0 <- a     		# set start value to estimate value
  xn <- n     		# we'll store the iterations of x_n here
  f_xn <- n   		# we'll store the iterations of f(x_n) here
  df_xn <- n		# derivative of f(x_n)
  
  # If the estimate results in f(x) = 0, then it is the root
  if (f(a) == 0.0) {
    cat("root = ", a, "\n")
  }

  # Store x_0 and f(x_0) as first item in the iterations
  xn[1] <- x0
  f_xn[1] <- f(x0)
  df_xn[1] <- 0
  for (i in 1:n) {
    # Get the derivative of f(x_n)
    dx <- genD(func = f, x = x0)$D[1]
	df_xn[i + 1] <- dx
    # Calculate next value for x_n
    x1 <- x0 - (f(x0) / dx)

    # Store next x and f(x)
    xn[i + 1] <- x1
    f_xn[i + 1] <- f(x1)

    # If x_n - x_n-1 is lesser than the tolerance, we have found the
    #   approximated root and we can now break the loop
    if (abs(x1 - x0) < tol) {
      foundRoot <- TRUE
      break
    }

    # If we haven't found a suitable root approximation yet,
    #   set x_n-1 to x_n and proceed with next iteration
    x0 <- x1
  }
  if (!foundRoot) {
    # If we haven't found the approx. root within the maximum number
    #   of iterations, display a message and exit.
    stop("Too many iterations reached without finding approx. root\n")
  } else {
    # Otherwise, display table.

    # Store approx. root in variable
    root <- tail(xn, n = 1)
    
    # Display the table
    cat("n\tx_n\t\tf(x_n)\t\tx_n - x_n-1\tf'(xn)\talpha - x_n - 1\n")
    for (i in 1:n) {
      if (i == 1) {
        # there are no (x_n - x_n-1) and (\alpha - x_n-1) for the first row
        #   so we only display n, x_n, and f(x_n).
        cat(0, "\t",
        format(xn[i], nsmall = 9), "\t",
        format(f_xn[i], nsmall = 9), "\n")
      } else {
        cat(i - 1, "\t",
        format(xn[i], nsmall = 9), "\t",
        format(f_xn[i], nsmall = 9), "\t",
        format(xn[i ] - xn[i - 1], nsmall = 9), "\t",
		format(df_xn[i], nsmall = 9), "\t",
        format(root - xn[i - 1]), "\n")
      }

      # stop displaying when we reach the last row
      if (xn[i] == root) {
        break
      }
    }
    cat("\napprox. root = ", format(root, nsmall = 9), "\n")
    cat("Accept x_n = ", format(root, nsmall = 9), " as root of f(x)\n")
  }
}

# Function for approximating root with the secant method
secant <- function(f, x0, x1, tol, n = 10000) {
  # Bool to flag when root approximation is found
  foundRoot <- FALSE
  
  # Begin calculating and displaying graph
  # Display first two iterates
  cat("n\tx_n\t\tf(x_n)\t\tx_n - x_(n - 1)\n")
  cat("0\t", x0, "\t\t", f(x0),"\n")
  cat("1\t",
      format(x1, nsmall = 9), "\t",
      format(f(x1), nsmall = 9), "\t",
      format(x1 - x0, nsmall = 9), "\n")
  for (i in 1:n) {
    # Iterative formula for secant method
    x2 <- x1 - f(x1) / ((f(x1) - f(x0)) / (x1 - x0))
    
    # Continue displaying iterates
    cat(i + 1, "\t",
        format(x2, nsmall = 9), "\t",
        format(f(x2), nsmall = 9), "\t",
        format(x2 - x1, nsmall = 9), "\n")
    # If difference between x_n and x_(n - 1) is small
    #   enough, accept x_n as suitable approx. root
    # Display iterate and root, then stop.
    if (abs(x2 - x1) < tol) {
      foundRoot <- TRUE
      cat("approx. root = ", format(x2, nsmall = 9), "\n")
      cat("Accept x_n = ", format(x2, nsmall = 9), " as root of f(x)\n")
      return()
    }

    # Otherwise, update variables, and continue calculating
    x0 <- x1
    x1 <- x2
  }

  if (!foundRoot) {
    stop("Too many iterations reached without finding approx. root\n")
  }
}

# script loops until user does not want to do another calculation
again <- TRUE

while(again) {
  # Prompt user to select approximation method
  cat("[1] Bisection Method\n")
  cat("[2] Newton's Method\n")
  cat("[3] Secant Method\n")
  cat("\nSelect method: ")
  approxMethod <- readLines("stdin", n = 1)

  if (!(approxMethod == "1" ||
        approxMethod == "2" ||
        approxMethod == "3")) {
    stop("Error: invalid input")
  } else {
    # Prompt user to enter function
    cat("Enter function: ")

    # Get user input and display the function entered
    func <- readLines("stdin", n = 1)
    print(paste("f(x) = ", func))
    cat("\n")

    # Parse the entered function using R's built-in
    #     parse() function
    f <- function(x){eval(parse(text=func))}

    # Display graph of function for user to visually get an estimate or interval
    curve(f, col = "blue", lwd = 2, n = 100, xlim = c(-10, 10), ylim = c(-10, 10), ylab="f(x)")
    abline(h = 0, v = 0)

    # Wait for user to continue
    pause <- readline(prompt="Press Enter after estimating from graph to continue...")
    graphics.off()

    if (approxMethod == "1") {
      # Prompt user to enter a value
      cat("Enter value for a: ")

      # Get user input and display a value
      a <- readLines("stdin", n = 1)
      print(paste("a = ", a))
      cat("\n")

      # Prompt user to enter b value
      cat("Enter value for b: ")

      # Get user input and display b value
      b <- readLines("stdin", n = 1)
      print(paste("b = ", b))
      cat("\n")

      # Convert a and b value to a number before evaluating
      a <- as.numeric(a)
      b <- as.numeric(b)
      
      # Prompt user to enter tolerance value
      cat("Enter tolerance value: ")

      # Get user input and display tolerance value 
      tol <- readLines("stdin", n = 1)
      print(paste("tolerance = ", tol))
      cat("\n")

      tol <- as.numeric(tol)

      bisection(f, a, b, tol)
    } else if (approxMethod == "2") {
      # Prompt user to enter estimate value
      cat("Enter estimate value: ")

      # Get user input and display estimate
      a <- readLines("stdin", n = 1)
      print(paste("x_0 = ", a))
      cat("\n")

      # Convert estimate value to a number before evaluating
      a <- as.numeric(a)

      # Prompt user to enter tolerance value
      cat("Enter tolerance value: ")

      # Get user input and display tolerance value 
      tol <- readLines("stdin", n = 1)
      print(paste("tolerance = ", tol))
      cat("\n")

      tol <- as.numeric(tol)

      newton(f, a, tol)
    } else if (approxMethod == "3") {
      # Prompt user to enter x_0 value
      cat("Enter value for x_0: ")

      # Get user input and display x_0 value
      x0 <- readLines("stdin", n = 1)
      print(paste("x_0 = ", x0))
      cat("\n")

      # Prompt user to enter x1 value
      cat("Enter value for x_1: ")

      # Get user input and display x1 value
      x1 <- readLines("stdin", n = 1)
      print(paste("x_1 = ", x1))
      cat("\n")

      # Convert a and b value to a number before evaluating
      x0 <- as.numeric(x0)
      x1 <- as.numeric(x1)
      
      # Prompt user to enter tolerance value
      cat("Enter tolerance value: ")

      # Get user input and display tolerance value 
      tol <- readLines("stdin", n = 1)
      print(paste("tolerance = ", tol))
      cat("\n")

      tol <- as.numeric(tol)

      secant(f, x0, x1, tol)
    }
  }
  doAgain <- readline(prompt="Enter 1 to do another calculation: ")
  ifelse (doAgain == "1",
          again <- TRUE,
          again <- FALSE)
}
