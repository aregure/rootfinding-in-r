# rootfinding-in-r

Bisection, Newton-Raphson, and secant rootfinding methods, implemented in an R script.

This requires the `numDeriv` package installed. Install it by running the following in an R console:

```r
    install.packages("numDeriv", dependencies=TRUE)
```

## Usage
1. Open an R console in your command-line terminal (`R` for Linux and macOS, `R.exe` for Windows).
2. Assuming you are already in the directory where the R script is located, run it using the following command:
    ```r
        source("RootApproximation.r")
    ```
   **Note:** This script will not execute properly in an interactive environment.
3. Select a rootfinding method.
4. Enter a mathematical function you wish to evaluate the root of, such as `sin(x) + cos(x)`.
5. A graph will appear. Use the graph to make your initial estimates.
6. Once you are done with making initial estimates from the graph, close the graph and press <kbd>Enter</kbd> on the console.
7. Enter an error tolerance value.
8. A table of iterates will then be calculated, and a root approximation will be accepted according to the error tolerance specified.
